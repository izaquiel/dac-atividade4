/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidade;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
@FacesConverter(value = "produtoConverter")
public class ProdutoConverter implements Converter{
    @EJB
    Dao<Produto> dao;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.equals("") || value.equalsIgnoreCase("--selecione--")) {
            return null;
        } else {            
            Map<String, Object> map = new HashMap<>();
            map.put("id", Integer.parseInt(value));
            Produto produto = dao.buscar("getProdutoPorId", map);
            return produto;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Produto) {
            Produto produto = (Produto) value;
            return String.valueOf(produto.getId());
        } else {
            return null;
        }
    }
}
