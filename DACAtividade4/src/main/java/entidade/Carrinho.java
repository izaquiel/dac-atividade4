/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidade;

import java.util.List;
import javax.ejb.Stateful;

/**
 *
 * @author Izaquiel
 */
@Stateful
public class Carrinho {
    
    Pedido pedido;

    public Carrinho() {
        pedido = new Pedido();
    }

    public Pedido getPedido() {
        return pedido;
    }
    

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }
    
    public List<Produto> getProdutos(){
        return pedido.getProdutos();
    }
}
