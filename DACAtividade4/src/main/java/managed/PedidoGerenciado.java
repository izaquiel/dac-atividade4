/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managed;

import entidade.Carrinho;
import entidade.Pedido;
import entidade.Produto;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
@ManagedBean(name = "Carrinho")
@SessionScoped
public class PedidoGerenciado implements Serializable{
    
    @EJB
    Carrinho carrinho;
    
    @EJB
    Dao<Pedido> pedidoDao;
    
    Produto produto = new Produto();
    
    List<Produto> produtos;
    
    public PedidoGerenciado() {
        produtos = new ArrayList<>();
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    
    
    public void addProdCarrinho(){
        System.out.println(produto);
        carrinho.getPedido().add(produto);
        produtos = carrinho.getProdutos();
    }
    
    public void finalizarPedido() throws IOException{
        pedidoDao.salvar(carrinho.getPedido());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
        FacesContext.getCurrentInstance().getExternalContext().redirect("cadastraCarrinho.jsp");
     
    }    
    
    public void remover(Produto p ){
        carrinho.getProdutos().remove(p);
        produtos = carrinho.getProdutos();
    }

    public Carrinho getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(Carrinho carrinho) {
        this.carrinho = carrinho;
    }


    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    
}
