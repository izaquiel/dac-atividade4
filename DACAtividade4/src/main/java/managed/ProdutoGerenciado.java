/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managed;

import entidade.Produto;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
@ManagedBean(name = "Produto")
public class ProdutoGerenciado {
    
    Produto produto = new Produto();
    @EJB
    Dao<Produto> produtoDao;
    
    public ProdutoGerenciado() {
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    public void addProduto(){
        produtoDao.salvar(produto);
        produto = new Produto();
    }
    

    public List<SelectItem> listaProdutos() {

        List<SelectItem> lista = new ArrayList<>();

        List<Produto> produtos = produtoDao.buscarTodos("buscaProdutos");

        for (Produto p : produtos) {
            lista.add(new SelectItem(p, p.getDescricao()));
        }
        return lista;
    }
    
}
